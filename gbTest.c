/*
 * Written By: Sebastian Ferngrne
 * For: GBJAM5 
 * Notes: 
 * Code written for gameboy jam5
 * Might be helpful for someone looking into GBDK 
 * 
 * This is what happens when "making it happen" is more important than structure
 * Tried cleaning up some commented out code and renaming some variables to 
 * actually be readable
 * 
 */

#include <gb/gb.h>
#include <rand.h>
#include <gb/drawing.h>
#include <stdio.h>

//Indexes into spritemap
#define FLAPPY_LEFT 0
#define FLAPPY_FACE 1 
#define PIPETOP 2
#define PIPEBOT 15 

#define WIN_W 160
#define WIN_H 144


static const UWORD FlyAnim[] = {0,16,20,22,22,24};
UBYTE gameFrame = 0;
UBYTE currentAnimationFrame = 0;
UBYTE ANIMATION_LENGTH = 5;

//Variables for flappys position and width 
UBYTE posX = 25;
UBYTE posY = 75; 
UBYTE playerW = 16; 
UBYTE score = 0; 
UBYTE idleFace = 2;

BYTE acc = 0; 
BYTE grav = 0;
BYTE maxGrav = 30;
UBYTE canJump; 

UBYTE keys = J_START;
UBYTE previousKeys = J_START;

typedef struct Pipe{
  UBYTE x;
  UBYTE height;
  UBYTE opening_y; 
  UBYTE index;
} sPipe; 

//Predefined pipes for spawning 'random'
const UBYTE NUM_PIPES = 5; 
const sPipe pipes[] = {
  {0,16*4,16 * 2,2} , //HIGH PIPE
  {0,16*4,16 * 3,2} , //HIGH PIPE
  {0,16*4,16 * 4,2} , //HIGH PIPE
  {0,16*4,16 * 4,2},  //MID PIPE
  {0,16*4,16 * 4,2}, //HIGH PIPE
  {0,16*4,16 * 5,2}   //LOW PIPE 
};

unsigned char Sprites[] = {
  0x1F,0x1F,0x3F,0x20,0xFF,0xC0,0xFF,0xC0,
  0xBF,0xA0,0x9F,0x90,0x9F,0x90,0x8F,0x88,
  0x8F,0x8C,0x87,0x84,0xC7,0xC5,0xCF,0xCD,
  0x7B,0x7D,0xC7,0xB9,0x7F,0x40,0x3F,0x3F,
  0xE0,0xE0,0xF8,0x38,0xE4,0x24,0xE2,0x62,
  0xC2,0x42,0x8A,0x8A,0x89,0x89,0x81,0x81,
  0x81,0x81,0xFF,0xFF,0x00,0xFF,0x7F,0xFF,
  0x00,0xFF,0xFF,0xFF,0x0E,0xFE,0xFC,0xFC,
  0xFF,0xFF,0x80,0xFF,0x80,0xFF,0x80,0xFF,
  0x80,0xFF,0xFF,0xC0,0x80,0xF2,0x80,0xF2,
  0x80,0xF2,0x80,0xF2,0x80,0xF2,0x80,0xF2,
  0x80,0xF2,0x80,0xF2,0x80,0xF2,0x80,0xF2,
  0xFF,0xFF,0x01,0xFF,0x01,0xFF,0x01,0xFF,
  0x01,0xFF,0xFB,0x07,0x01,0xFF,0x01,0xFF,
  0x01,0xFF,0x01,0xFF,0x01,0xFF,0x01,0xFF,
  0x01,0xFF,0x01,0xFF,0x01,0xFF,0x01,0xFF,
  0x80,0xF2,0x80,0xF2,0x80,0xF2,0x80,0xF2,
  0x80,0xF2,0x80,0xF2,0x80,0xF2,0x80,0xF2,
  0x80,0xF2,0x80,0xF2,0xFF,0xC0,0x80,0xFF,
  0x80,0xFF,0x80,0xFF,0x80,0xFF,0xFF,0xFF,
  0x01,0xFF,0x01,0xFF,0x01,0xFF,0x01,0xFF,
  0x01,0xFF,0x01,0xFF,0x01,0xFF,0x01,0xFF,
  0x01,0xFF,0x01,0xFF,0xFB,0x07,0x01,0xFF,
  0x01,0xFF,0x01,0xFF,0x01,0xFF,0xFF,0xFF,
  0x80,0xF2,0x80,0xF2,0x80,0xF2,0x80,0xF2,
  0x80,0xF2,0x80,0xF2,0x80,0xF2,0x80,0xF2,
  0x80,0xF2,0x80,0xF2,0x80,0xF2,0x80,0xF2,
  0x80,0xF2,0x80,0xF2,0x80,0xF2,0x80,0xF2,
  0x01,0xFF,0x01,0xFF,0x01,0xFF,0x01,0xFF,
  0x01,0xFF,0x01,0xFF,0x01,0xFF,0x01,0xFF,
  0x01,0xFF,0x01,0xFF,0x01,0xFF,0x01,0xFF,
  0x01,0xFF,0x01,0xFF,0x01,0xFF,0x01,0xFF,
  0x1F,0x1F,0x3F,0x20,0x7F,0x40,0xFF,0xC0,
  0xFF,0x80,0xFF,0x80,0xFF,0x80,0xFF,0x80,
  0xFF,0xBC,0xE7,0xA6,0xC1,0xC1,0xC1,0xC1,
  0x83,0x83,0x8D,0x8F,0xB8,0xBF,0xFF,0xFF,
  0xE0,0xE0,0xF8,0x38,0xE4,0x24,0xE2,0x62,
  0xC2,0x42,0x8A,0x8A,0x89,0x89,0x95,0x95,
  0x81,0x81,0xFF,0xFF,0x00,0xFF,0x7F,0xFF,
  0x48,0xFF,0xCF,0xFF,0x4E,0xFE,0xFC,0xFC,
  0x1F,0x1F,0x3F,0x20,0x7F,0x40,0xFF,0xC0,
  0xFF,0x80,0xFF,0x80,0xFF,0x80,0xFF,0x80,
  0xFF,0xBC,0xE7,0xA6,0xC1,0xC1,0xC1,0xC1,
  0xC3,0xC3,0xED,0xAF,0x78,0x7F,0x3F,0x3F,
  0x1F,0x1F,0x3F,0x20,0x7F,0x40,0xFF,0xC0,
  0xFF,0x80,0xFF,0x80,0xFF,0x80,0xFF,0x80,
  0xFF,0xBC,0xE7,0xA6,0xC3,0xC3,0xC5,0xC7,
  0xF9,0xFF,0xC1,0xBF,0x60,0x7F,0x3F,0x3F,
  0x1F,0x1F,0x3F,0x20,0x7F,0x40,0xFF,0xC0,
  0xFF,0x80,0xFF,0x80,0xFF,0xB8,0xCF,0xCC,
  0xC7,0xC6,0xC3,0xC2,0xC3,0xC3,0xC5,0xC7,
  0xF9,0xFF,0xC1,0xBF,0x60,0x7F,0x3F,0x3F,
  0x10,0x10,0x30,0x30,0x10,0x10,0x10,0x10,
  0x10,0x10,0x38,0x38,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x3C,0x3C,0x42,0x42,0x04,0x04,0x18,0x18,
  0x20,0x20,0x7E,0x7E,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x3C,0x3C,0x42,0x42,0x04,0x04,0x18,0x18,
  0x42,0x42,0x3C,0x3C,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x08,0x08,0x18,0x18,0x28,0x28,0x48,0x48,
  0x7C,0x7C,0x08,0x08,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x7E,0x7E,0x40,0x40,0x7C,0x7C,0x02,0x02,
  0x42,0x42,0x3C,0x3C,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x3C,0x3C,0x40,0x40,0x7C,0x7C,0x42,0x42,
  0x42,0x42,0x3C,0x3C,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x7E,0x7E,0x04,0x04,0x08,0x08,0x10,0x10,
  0x20,0x20,0x40,0x40,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x3C,0x3C,0x42,0x42,0x3C,0x3C,0x42,0x42,
  0x42,0x42,0x3C,0x3C,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x3C,0x3C,0x42,0x42,0x42,0x42,0x3E,0x3E,
  0x02,0x02,0x3C,0x3C,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x38,0x38,0x44,0x44,0x44,0x44,0x44,0x44,
  0x44,0x44,0x38,0x38,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xE0,0xE0,0xF8,0x38,0xE4,0x24,0xE2,0x62,
  0xCA,0x4A,0xCA,0x4A,0xFF,0x7F,0xC1,0x7F,
  0xBE,0xFE,0xA0,0xE0,0x20,0xE0,0x3E,0xFE,
  0x01,0xFF,0xFF,0xFF,0xFE,0x06,0xFC,0xFC,
  0xE0,0xE0,0xF8,0x38,0xE4,0x24,0xEA,0x6A,
  0xCA,0x4A,0x82,0x82,0x81,0x81,0x81,0x81,
  0x81,0x81,0xFF,0xFF,0x00,0xFF,0x7F,0xFF,
  0x00,0xFF,0xFF,0xFF,0x0E,0xFE,0xFC,0xFC,
  0xE0,0xE0,0xF8,0x18,0xFC,0x3C,0xE2,0x62,
  0xC2,0x42,0x82,0x82,0x81,0x81,0x89,0x89,
  0x89,0x89,0xFF,0xFF,0x00,0xFF,0x7F,0xFF,
  0x00,0xFF,0xFF,0xFF,0x0E,0xFE,0xFC,0xFC,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
}; 

/*Func Declarations*/
UBYTE CheckCollision(UBYTE x, UBYTE y, struct Pipe* pipe);
void ResetPipe(struct Pipe* pipe,UBYTE offset);

void clearScreen()
{
  printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
}

void StartScreen(){
  printf("                   \n");
  printf("                   \n");
  printf("   Flopping Bird   \n");
  printf("   Made for GBJAM5 \n");
  printf("                   \n");
  printf("   Press Jump/Up   \n");
  printf("   To Begin        \n");
  printf("                   \n");
  printf("                   \n");
  printf("   By: Chebastian  \n");
  waitpad(J_UP);
  clearScreen();
}

void DeathScreen(){
  clearScreen(); 
  printf("   Score: %d       \n",(UWORD)score);
  printf("   Press Jump/Up   \n");
  printf("   To Retry!       \n");
  printf("                   \n"); // push it up to 'mid' screen
  printf("                   \n"); 
  printf("                   \n");
  printf("                   \n");
  waitpad(J_UP); 
  clearScreen(); 
} 

void DrawPipe(struct Pipe* pipe){
  UBYTE i;
  UBYTE index = pipe->index;

  set_sprite_tile(PIPETOP + index,8);
  move_sprite(PIPETOP + index,pipe->x,pipe->opening_y-16); 

  index += 1;
  set_sprite_tile(PIPETOP + index,10);
  move_sprite(PIPETOP + index,pipe->x + 8,pipe->opening_y-16); 

  index += 1; 
  for(i = 0; i < pipe->opening_y - 16; i += 16){
    set_sprite_tile(PIPETOP+index,12);
    move_sprite(PIPETOP+index,pipe->x,i); 

    index = index + 1;
    set_sprite_tile(PIPETOP+index,14);
    move_sprite(PIPETOP+index,pipe->x + 8,i); 
    index = index + 1;
  }

  for(i = pipe->opening_y + pipe->height + 16; i <= WIN_H+16; i += 16){
    index = index + 1;
    set_sprite_tile(PIPETOP+index,12);
    move_sprite(PIPETOP+index,pipe->x,i); 

    index = index + 1;
    set_sprite_tile(PIPETOP+index,14);
    move_sprite(PIPETOP+index,pipe->x + 8,i); 
  }


  index = index + 1;
  set_sprite_tile(index,4);
  move_sprite(index,pipe->x,pipe->opening_y + pipe->height); 

  index = index + 1;
  set_sprite_tile(index,6);
  move_sprite(index,pipe->x + 8,pipe->opening_y + pipe->height); 
}

UBYTE GetFlyFrame()
{
  if((1+gameFrame) % 3 == 0)
    currentAnimationFrame++;

  if(currentAnimationFrame > ANIMATION_LENGTH)
    currentAnimationFrame = 0; 

  if(grav > -20)
    return 0;

  return FlyAnim[currentAnimationFrame];
}

UBYTE GetFace()
{
    if(grav < 20)
    {
      return idleFace;
    }

  return 46;
}

void DrawDead(UBYTE x ,UBYTE y)
{
  set_sprite_tile(0,GetFlyFrame());
  move_sprite(FLAPPY_LEFT,x,y);

  set_sprite_tile(1,18);
  move_sprite(FLAPPY_FACE,x+8,y); 
}

void DrawFlappy(UBYTE x ,UBYTE y)
{
  set_sprite_tile(0,GetFlyFrame());
  move_sprite(FLAPPY_LEFT,x,y);

  set_sprite_tile(1,GetFace());
  move_sprite(FLAPPY_FACE,x+8,y); 
}

//Why: Cause i wanted to update the pipe to move X pixles, every Y frame
//But it does look like we are moving it 2 frames every second frame, so thats usefull...
UBYTE maxy; 
UBYTE pipeSpeed = 2;
UBYTE updateFrame = 2;
UBYTE frameCount  = 0;

void AdvanceFrame()
{
  frameCount++; 
}

void ResetFrame()
{
    if(frameCount >= updateFrame)
    {
	frameCount = 0;
    } 
}

void PrintScore()
{
    UBYTE scoreY = 152;
    UBYTE numberTile = 24; 
    UBYTE SPR_NUM1 = 38;
    UBYTE SPR_NUM2 = 39;
    BYTE scoreTens = (score) / 10;
    UBYTE scoreX = score;

    if(scoreTens > 0){
	numberTile += ((scoreTens) *2); 
	if(scoreTens == 0)
	    numberTile = 44;

	set_sprite_tile(SPR_NUM1,numberTile);
	move_sprite(SPR_NUM1,10,scoreY);
	scoreX = ((score - (scoreTens*10)));
	numberTile = 24;
    }

    numberTile += (scoreX *2); 
    if(scoreX == 0)
      numberTile = 44;

    set_sprite_tile(SPR_NUM2,numberTile);
    move_sprite(SPR_NUM2,20,scoreY); 
}

void UpdatePipe(struct Pipe* pipe)
{
    if(frameCount >= updateFrame)
    {
	if(pipe->x + 16 > posX && (pipe->x - pipeSpeed) + 16 < posX)
	{
	  score++;
	}
	pipe->x -= pipeSpeed; 
    }


    maxy = WIN_H - pipe->height - 16; 
    if(pipe->x + 16 <= 0)
    {
      ResetPipe(pipe,0);
    } 
} 

/*offset never really used*/
void ResetPipe(struct Pipe* pipe, UBYTE offset){
    UBYTE index = rand() % NUM_PIPES;
    pipe->x = WIN_W + 16;
    DrawPipe(pipe);
    if(index > NUM_PIPES)
    index = NUM_PIPES -1;

    pipe->x  = 16 + WIN_W + offset;
    pipe->opening_y = pipes[index].opening_y; 
}

/*DEBUG FUNCTION*/
void FreeMovePlayer(UBYTE key, UBYTE oldKey, struct Pipe* pipe )
{
  UBYTE nextX, nextY;

  nextX = posX;
  nextY = posY;
  if(key & J_LEFT)
    nextX -= 1;
  else if(key & J_RIGHT)
    nextX += 1;
  else if(key & J_UP)
    nextY -= 1;

  if(key & J_DOWN)
    nextY += 1;

  if(CheckCollision(nextX, nextY,pipe) == 0){
    posX = nextX;
    posY = nextY; 
  }
}

void UpdatePlayer(UBYTE key, UBYTE oldKey){
    canJump = (key & J_UP)  && !(oldKey & J_UP);
    if(canJump){
      grav = -50;
    }
    else if(key & J_DOWN){
      posY += 1; 
    }
    else {
      acc = 2; 
    }

    grav += acc;

    if(grav > maxGrav)
      grav = maxGrav;

    posY += 1 + (grav / 10);
} 

UBYTE CheckCollision(UBYTE x, UBYTE y, struct Pipe* pipe)
{
  UBYTE pw,ph;
  pw = 8; 
  ph = 16;
    if(y < pipe->opening_y || y > pipe->opening_y + pipe->height || y + ph > pipe->opening_y + pipe->height)
    {
      if((x > pipe->x || (x + playerW) > pipe->x) && (x < pipe->x + (pw*2) || x + pw < pipe->x))
	return 1; 
    } 
  return 0;
} 

void LookAtPipe(UBYTE x, UBYTE y, struct Pipe* pipe){
    UBYTE pw,ph;
    pw = 8; 
    ph = 16;

    if((x > pipe->x || (x + playerW) > pipe->x) && (x < pipe->x + (pw*2) || x + pw < pipe->x))
    {
      BYTE dist = (posY - pipe->opening_y);
      BYTE distB = ((pipe->opening_y + pipe->height) - (posY+ph)); 
      if(dist < distB)
	idleFace = 48;
      else
	idleFace = 50;
    }
    else if(idleFace <= 2)
      idleFace = 2;
  
}

void onDeath(struct Pipe* pipe, struct Pipe* pipe2)
{
  DrawDead(posX,posY);
  posY = 30; 
  pipe->x = WIN_W;
  pipe2->x = WIN_W ;
  DeathScreen();
  score = 0;
  ResetPipe(pipe,0);
  ResetPipe(pipe2,0);
  pipe2->x = 100;
}

void main(){

  UBYTE i;
  char str[8];
  struct Pipe pipe1,pipe2;

  pipe1.opening_y = pipes[0].opening_y;
  pipe1.height = 55;
  pipe1.x = WIN_W;
  pipe1.index = 2;

  pipe2.x = 70;
  pipe2.height = 55;
  pipe2.opening_y = 16  * 5;
  pipe2.index = 20;

  //SPRITES_8x8;
  SPRITES_8x16;
  set_sprite_data(0,100,Sprites);

  enable_interrupts();

  SHOW_SPRITES; 
  keys = previousKeys = joypad();

  StartScreen();

  while(1){
    wait_vbl_done();
    gameFrame++;
    gameFrame = gameFrame % 100;


    AdvanceFrame();
    UpdatePipe(&pipe1);
    UpdatePipe(&pipe2);
    ResetFrame();
    
    DrawPipe(&pipe1); 
    DrawPipe(&pipe2); 

    keys = joypad();
    DrawFlappy(posX,posY); 
    PrintScore();
    UpdatePlayer(keys, previousKeys);

    idleFace = 2;
    if(CheckCollision(posX, posY,&pipe1) || CheckCollision(posX, posY, &pipe2))
    {
      onDeath(&pipe1, &pipe2);
    } 

    LookAtPipe(posX, posY, &pipe1);
    LookAtPipe(posX, posY,&pipe2);

    if(posY > WIN_H || posY <= 0){
      onDeath(&pipe1,&pipe2);
    }

    SHOW_SPRITES; 
    previousKeys = keys; 
  } 
}
